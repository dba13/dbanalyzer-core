package com.db.demomidtier;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.sql.*;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class LoginControllerTest {

    @Mock
    private Connection jdbcConnection;

    @Mock
    private ResultSet resultSet;

    @Mock
    private ResultSet emptyResultSet;

    @Mock
    private Statement statement;

    @Mock
    private PreparedStatement preparedStatement;

    @Mock
    private ResultSetMetaData resultSetMetaData;

    @Mock
    private ResultSetMetaData emptyResultSetMetaData;

    private String pattern="SELECT user_id, user_pwd FROM users WHERE user_id=? and user_pwd=?";
    private String query = "SELECT user_id, user_pwd FROM users WHERE user_id=test_user and user_pwd=test_password";
    private String invalidQuery = "SELECT user_id, user_pwd FROM users WHERE user_id=test and user_pwd=test";

    @Before
    public void setUp() throws Exception {
        resultSet = Mockito.mock(ResultSet.class);
        resultSetMetaData = Mockito.mock(ResultSetMetaData.class);
        Mockito.when(resultSet.next()).thenReturn(true).thenReturn(false);
        Mockito.when(resultSet.getObject(1)).thenReturn("test");
        Mockito.when(resultSet.getObject(2)).thenReturn("test");
        Mockito.when(resultSet.getMetaData()).thenReturn(resultSetMetaData);
        Mockito.when(resultSet.getMetaData().getColumnCount()).thenReturn(2);
        Mockito.when(resultSet.getMetaData().getColumnLabel(1)).thenReturn("user_id");
        Mockito.when(resultSet.getMetaData().getColumnLabel(2)).thenReturn("user_pwd");

        emptyResultSet = Mockito.mock(ResultSet.class);
        emptyResultSetMetaData = Mockito.mock(ResultSetMetaData.class);
        Mockito.when(emptyResultSet.getMetaData()).thenReturn(emptyResultSetMetaData);
        Mockito.when(emptyResultSet.getMetaData().getColumnCount()).thenReturn(0);
        Mockito.when(emptyResultSet.next()).thenReturn(false);

        statement = Mockito.mock(Statement.class);
        Mockito.when(statement.executeQuery(query)).thenReturn(resultSet);
        Mockito.when(statement.executeQuery(invalidQuery)).thenReturn(emptyResultSet);

        preparedStatement = Mockito.mock(PreparedStatement.class);
        Mockito.when(preparedStatement.executeQuery()).thenReturn(resultSet);

        jdbcConnection = Mockito.mock(Connection.class);
        Mockito.when(jdbcConnection.createStatement()).thenReturn(statement);
        Mockito.when(jdbcConnection.prepareStatement(pattern)).thenReturn(preparedStatement);
        Mockito.when(jdbcConnection.isValid(1)).thenReturn(true);
    }

    @Test
    public void test_connection(){
        LoginController loginController = new LoginController(jdbcConnection);
        assertTrue(loginController.isConnected());
    }

    @Test
    public void test_getter(){
        LoginController loginController = new LoginController(jdbcConnection);
        assertThat(loginController.databaseAddress(),is("jdbc:mysql://10.0.75.1:3307/"));
        //loginController = new LoginController();
        assertThat(loginController.databaseAddress(),is("jdbc:mysql://10.0.75.1:3307/"));
    }

    @Test
    public void test_valid_login(){
        LoginController loginController = new LoginController(jdbcConnection);
        assertThat(loginController.loginCheck("test_user","test_password"),is("[{\"user_id\":\"test\",\"user_pwd\":\"****\"}]"));
    }

    @Test
    public void test_invalid_login() throws SQLException {
        Mockito.reset(preparedStatement);
        Mockito.reset(jdbcConnection);

        preparedStatement = Mockito.mock(PreparedStatement.class);
        Mockito.when(preparedStatement.executeQuery()).thenReturn(emptyResultSet);

        jdbcConnection = Mockito.mock(Connection.class);
        Mockito.when(jdbcConnection.prepareStatement(pattern)).thenReturn(preparedStatement);
        Mockito.when(jdbcConnection.isValid(1)).thenReturn(true);


        LoginController loginController = new LoginController(jdbcConnection);
        assertNull(loginController.loginCheck("test", "test"));
    }

    @Test
    public void test_login_without_connection() throws Exception{
        Mockito.reset(jdbcConnection);
        jdbcConnection = Mockito.mock(Connection.class);
        Mockito.when(jdbcConnection.prepareStatement(pattern)).thenReturn(preparedStatement);
        Mockito.when(jdbcConnection.isValid(1)).thenReturn(false);

        LoginController loginController = new LoginController(jdbcConnection);
        assertNull(loginController.loginCheck("test", "test"));
    }

    @Test
    public void test_exception() throws Exception{
        Mockito.reset(jdbcConnection);
        jdbcConnection = Mockito.mock(Connection.class);
        Mockito.when(jdbcConnection.isValid(1)).thenReturn(true);
        Mockito.when(jdbcConnection.prepareStatement(pattern)).thenThrow(new SQLException());

        LoginController loginController = new LoginController(jdbcConnection);
        assertNull(loginController.loginCheck("test", "test"));
    }
}